const {Router} = require('express');
const config = require('config');
const shortId = require('shortid');
const Link = require('../models/Link');
const authMiddleware = require('../middleware/auth.middleware');
const router = Router();

const requestHandlerCreator = (handler) => {
   return async (req, res) => {
      try {
         await handler(req, res);
      } catch ({message = 'Что-то пошло не так. Попробуйте снова.'}) {
         res.status(500).json({message: message});
      }
   };
};

router.post('/generate', authMiddleware, requestHandlerCreator(async (req, res) => {
   const baseUrl = config.get('baseUrl');
   const {from} = req.body;
   const code = shortId.generate();
   
   const existing = await Link.findOne({from});
   
   if (existing) {
      return res.json({link: existing});
   }
   
   const to = baseUrl + '/t/' + code;
   const link = new Link({
      code,
      to,
      from,
      owner: req.user.userId
   });
   await link.save();
   
   res.status(201).json({
      link
   });
}));

router.get('/', authMiddleware, requestHandlerCreator(async (req, res) => {
   const links = await Link.find({
      owner: req.user.userId
   });
   res.json(links);
}));

router.get('/:id', authMiddleware, requestHandlerCreator(async (req, res) => {
   const links = await Link.findById(req.params.id);
   res.json(links);
}));

module.exports = router;
