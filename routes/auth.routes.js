const {Router} = require('express');
const UserModel = require('../models/User');
const {check, validationResult} = require('express-validator');
const bcrypt = require('bcrypt');
const config = require('config');
const jwt = require('jsonwebtoken');
const  router = Router();

// /api/auth/register
router.post(
    '/register',
    [
        check('email', 'Введен некорректный email.').isEmail(),
        check('password', 'Минимальная длина пароля 6 символов').isLength({min: 6})
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Введены некорректные данные при регистрации'
                });
            }

            const {email, password} = req.body;
            const candidate = await UserModel.findOne({ email });
            
            if (candidate) {
                return res.status(400).json({message: 'Пользователь с таким email уже зарегистрирован.'});
            }
    
            const hashedPassword = await bcrypt.hash(password, 11);
            
            const user = new UserModel({email, password: hashedPassword});
    
            await user.save();

            res.status(201).json({message: 'Пользователь создан!'});

        } catch (e) {
            console.log('Ыррор:', e.message);
            res.status(500).json({message: 'Что-то пошло не так. Попробуйте снова.'});
        }
    }
);

// /api/auth/login
router.post(
    '/login',
    [
        check('email', 'Введите корректный email.').normalizeEmail().isEmail(),
        check('password', 'Введите пароль').exists()
    ],
    async (req, res) => {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return res.status(400).json({
                    errors: errors.array(),
                    message: 'Введены некорректные данные при входе в систему'
                });
            }

            const {email, password} = req.body;

            const user = await UserModel.findOne({email});

            if (!user) {
                return res.status(400).json({message: 'Неверно введен email/пароль.'})
            }

            const isMatch = await bcrypt.compare(password, user.password);

            if (!isMatch) {
                return res.status(400).json({message: 'Неверно введен email/пароль.'})
            }

            const token = jwt.sign(
                {userId: user.id},
                config.get('jwtSecret'),
                {expiresIn: '1h'}
            );

            return res.json({token, userId: user.id});

        } catch (e) {
            res.status(500).json({message: 'Что-то пошло не так. Попробуйте снова.'});
        }
    }
);

module.exports = router;