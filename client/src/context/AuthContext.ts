import {createContext} from "react";

const emptyFunction = () => {};

export interface IAuthData {
    token: string;
    userId: string;
    login: (jwtToken: string, userId: string) => void,
    logout: () => void,
    isAuthenticated?: boolean
}

const contextInitValue: IAuthData = {
    token: '',
    userId: '',
    login: emptyFunction,
    logout: emptyFunction,
    isAuthenticated: false
};

export const AuthContext = createContext(contextInitValue);
