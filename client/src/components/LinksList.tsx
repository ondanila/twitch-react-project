import React from 'react';
import {Table} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import {ILink} from './LinkCard';

export const LinksList = ({links}: {links: ILink[]}) => {
    return (
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>Номер</th>
                    <th>Оригинал</th>
                    <th>Сокращенная</th>
                    <th>Открыть</th>
                </tr>
            </thead>
            <tbody>
                {
                    links.map(({from, to, _id}, index) => {
                        return (
                            <tr>
                                <td>{index}</td>
                                <td>{from}</td>
                                <td>{to}</td>
                                <td><Link to={`/detail/${_id}`}>Открыть</Link></td>
                            </tr>
                        );
                    })
                }
            </tbody>
        </Table>
    );
};