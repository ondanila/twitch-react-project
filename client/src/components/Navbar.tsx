import React, {useContext} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import {AuthContext} from '../context/AuthContext';

export default () => {
    const {logout} = useContext(AuthContext);
    //@ts-ignore
    const logoutHandler = (event) => {
        event.preventDefault();
        logout();
    };
    return (
        <Navbar bg="dark" variant="dark">
            <Navbar.Brand href="/">Сократи ссылку</Navbar.Brand>
            <Nav className="ml-auto">
                <LinkContainer to="/create">
                    <Nav.Link>Создать</Nav.Link>
                </LinkContainer>
                <LinkContainer to="/links">
                    <Nav.Link>Ссылки</Nav.Link>
                </LinkContainer>
                <Nav.Link onClick={logoutHandler}>Выйти</Nav.Link>
            </Nav>
        </Navbar>
    );
}