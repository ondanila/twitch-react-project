import React from 'react';
import './LinkCard.scss';

export interface ILink {
    _id: string,
    from: string,
    to: string,
    code: string,
    date: string,
    clicks: number,
    owner: string
}

export interface ILinkCardProps {
    linkData: ILink
}

export default function LinkCard({linkData: {to, from, clicks, date}}: ILinkCardProps) {
    return (
        <div className="linkCard">
            <h2>Ссылка</h2>
            <p>Ваша ссылка: <a href={to} target={to}>{to}</a></p>
            <p>Изначальная ссылка: <a href={from} target={to}>{from}</a></p>
            <p>Количество кликов: <strong>{clicks}</strong></p>
            <p>Дата создания: <strong>{new Date(date).toLocaleDateString()}</strong></p>
        </div>
    );
}