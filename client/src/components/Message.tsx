import React from "react";
import Toast from "react-bootstrap/Toast";
import './Message.scss';

export const MESSAGE_POPUP_TYPE = {
    ERROR: 'error',
    SUCCESS: 'success',
    INFO: 'info'
};

interface IMessagePopupProps {
    type: string,
    message: string,
    closeHandler: () => void
}

export const Message = ({
    type = 'info',
    message,
    closeHandler = () => {}
}: IMessagePopupProps) => {
    return (
        <Toast
            className="messagePopup"
            onClose={closeHandler}
            show={true}
            delay={3000}
            autohide>
            <Toast.Body className={`messagePopup-Body__${type}`}>{message}</Toast.Body>
        </Toast>
    );
};