import React, {FormEvent, useContext, useState} from "react";
import {Form, Button} from 'react-bootstrap';
import './AuthPage.scss';
import {useHttp} from "../hooks/http.hook";
import {Message, MESSAGE_POPUP_TYPE} from "../components/Message";
import {AuthContext} from "../context/AuthContext";

export const AuthPage = () => {
    const {login} = useContext(AuthContext);
    const [formData, setFromData] = useState({
        email: '',
        password: ''
    });
    const [messageData, setMessageData] = useState({
        type: 'info',
        message: ''
    });
    const {request, loading} = useHttp();

    const changeHandler = (event: FormEvent<HTMLInputElement>) => {
        // @ts-ignore
        setFromData({ ...formData, [event.target.name]: event.target.value });
    };

    const messageCloseHandler = () => {
        setMessageData({
            type: '',
            message: ''
        });
    };

    const loginHandler = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            const data = await request('/api/auth/login', 'POST', formData);
            login(data.token, data.userId);
        } catch (e) {
            setMessageData({
                type: MESSAGE_POPUP_TYPE.ERROR,
                message: e.message
            });
        }
    };

    const registerHandler = async () => {
        try {
            const data = await request('/api/auth/register', 'POST', formData);
            setMessageData({
                type: MESSAGE_POPUP_TYPE.SUCCESS,
                message: data.message
            });
        } catch (e) {
            setMessageData({
                type: MESSAGE_POPUP_TYPE.ERROR,
                message: e.message
            });
        }
    };

    return (
        <div className="auth">
            <div className="auth-MainHeader">Сократи ссылку</div>
            <div className="auth-Form">
                <div className="auth-Form__header">Авторизация</div>
                <Form onSubmit={loginHandler}>
                    <Form.Group controlId="email">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                            name="email"
                            value={formData.email}
                            onChange={changeHandler}
                            placeholder="Введите email"
                            type="email"/>
                    </Form.Group>
                    <Form.Group controlId="password">
                        <Form.Label>Пароль</Form.Label>
                        <Form.Control
                            name="password"
                            value={formData.password}
                            onChange={changeHandler}
                            placeholder="Введите пароль"
                            type="password"/>
                    </Form.Group>
                    <Button
                        className="auth-Form__button"
                        disabled={loading}
                        variant="success"
                        type="submit">
                        Войти
                    </Button>
                    <Button
                        className="auth-Form__button"
                        disabled={loading}
                        variant="outline-primary"
                        onClick={registerHandler}>
                        Регистрация
                    </Button>
                </Form>
            </div>
            {
                messageData.message &&
                <Message
                   type={messageData.type}
                   message={messageData.message}
                   closeHandler={messageCloseHandler}/>
            }
        </div>
    );
};
