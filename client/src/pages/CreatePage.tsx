import React, {FormEvent, useState} from 'react';
import {Button, Form} from 'react-bootstrap';
import {useHistory} from 'react-router';
import {useHttp} from '../hooks/http.hook';
import './CreatePage.scss';

export const CreatePage = () => {
    const [link, setLink] = useState('');
    const {push} = useHistory();
    const {request}  = useHttp();
    
    // @ts-ignore
    const linkChangedHandler = (event:FormEvent<HTMLInputElement>) => setLink(event.target.value);
    
    const submitFormHandler = async (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        try {
            const data = await request('/api/link/generate', 'POST', {from: link});
            push(`/detail/${data.link._id}`);
        } catch (e) {
            console.error(e);
        }
    };
    
    return (
        <div className="createLink">
            <Form onSubmit={submitFormHandler}>
                <Form.Group controlId="link">
                    <Form.Label>Введите ссылку</Form.Label>
                    <div className="createLink-inputContainer">
                        <Form.Control
                            name="link"
                            value={link}
                            onChange={linkChangedHandler}
                            placeholder="Введите ссылку"
                            type="text"/>
                        <Button
                            className="createLink-Form__button"
                            variant="primary"
                            type="submit">
                            Принять
                        </Button>
                    </div>
                </Form.Group>
            </Form>
        </div>
    );
};
