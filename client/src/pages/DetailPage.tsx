import React, {useCallback, useEffect, useState} from 'react';
import {useParams} from 'react-router';
import {useHttp} from '../hooks/http.hook';
import LinkCard from '../components/LinkCard';


export const DetailPage = () => {
    const {request, loading} = useHttp();
    const [link, setLink] = useState();
    const {id: linkId} = useParams();
    
    const getLink = useCallback(async () => {
        try {
            const fetched = await request(`/api/link/${linkId}`, 'GET');
            setLink(fetched);
        } catch (e) {}
    }, [linkId, request, setLink]);
    
    useEffect(() => {
        getLink();
    }, [getLink]);
    
    if (loading || !link) {
        return <></>;
    }
    
    return (
        <LinkCard linkData={link}/>
    );
};
