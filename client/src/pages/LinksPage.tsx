import React, {useCallback, useEffect, useState} from 'react';
import {LinksList} from '../components/LinksList';
import {useHttp} from '../hooks/http.hook';


export const LinksPage = () => {
    const {request, loading} = useHttp();
    const [links, setLinks] = useState();
    
    const getLinks = useCallback(async () => {
        try {
            const fetched = await request(`/api/link`, 'GET');
            setLinks(fetched);
        } catch (e) {}
    }, [request, setLinks]);
    
    useEffect(() => {
        getLinks();
    }, [getLinks]);
    
    if (loading || !links) {
        return <></>;
    }
    
    return (
        <LinksList links={links}/>
    );
};
