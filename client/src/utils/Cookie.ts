export const setCookie = (name: string, value: any, maxAge: number): void => {
   document.cookie = `${name}=${value}; max-age=${maxAge};`;
};

export const getCookie = (name: string): string => {
   let matches = document.cookie.match(new RegExp(
       "(?:^|; )" + name.replace(/([.$?*|{}()[\]\\/+^])/g, '\\$1') + "=([^;]*)"
   ));
   return matches ? decodeURIComponent(matches[1]) : '';
};

export const removeCookie = (name: string) => {
   setCookie(name, '', -1);
};
