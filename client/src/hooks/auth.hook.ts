import {useState, useCallback} from 'react';
import {setCookie, removeCookie, getCookie} from "../utils/Cookie";
import {IAuthData} from "../context/AuthContext";

export const AUTH_COOKIE_NAME = 'react-twitch-auth';
const AUTH_COOKIE_MAX_AGE = 3600;

export const useAuth = (): IAuthData => {
    const currentAuthCookie: string = getCookie(AUTH_COOKIE_NAME);
    let currentAuthData = {
        token: '',
        userId: ''
    };

    if (currentAuthCookie) {
        currentAuthData = JSON.parse(currentAuthCookie);
    }

    const [token, setToken] = useState(currentAuthData.token);
    const [userId, setUserId] = useState(currentAuthData.userId);

    const login = useCallback((jwtToken: string, id: string) => {
        setToken(jwtToken);
        setUserId(id);
        if (jwtToken) {
            setCookie(AUTH_COOKIE_NAME, JSON.stringify({
                token: jwtToken,
                userId: id
            }), AUTH_COOKIE_MAX_AGE);
        }
    }, []);

    const logout = useCallback(() => {
        setToken('');
        setUserId('');
        removeCookie(AUTH_COOKIE_NAME);
    }, []);

    return { login, logout, token, userId };
};
