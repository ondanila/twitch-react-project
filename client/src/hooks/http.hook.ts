import {useState, useCallback} from 'react'
import {getCookie} from '../utils/Cookie';
import {AUTH_COOKIE_NAME} from './auth.hook';

export const useHttp = () => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const request = useCallback(
        async (
            url: string,
            method: string = 'GET',
            body: object|null = null,
            headers: any = {}
        ) => {
            setLoading(true);
            try {
                let strBody: string = '';
                if (body) {
                    strBody = JSON.stringify(body);
                    headers['Content-Type'] = 'application/json'
                }
                
                // Добавляем заголовок с данными авторизации для использования на бл.
                if (!headers.Authorization) {
                    const authCookie = getCookie(AUTH_COOKIE_NAME);
                    if (authCookie) {
                        const authObject = JSON.parse(authCookie);
                        headers.Authorization = `Bearer ${authObject.token}`;
                    }
                }
                
                const requestParams: {method: string, body?: string, headers: any} = {method, headers};

                if (strBody) {
                    requestParams.body = strBody;
                }
                
                const response = await fetch(url, requestParams);
                const data = await response.json();

                if (!response.ok) {
                    throw new Error(data.message || 'Что-то пошло не так')
                }

                setLoading(false);

                return data
            } catch (e) {
                setLoading(false);
                setError(e.message);
                throw e
            }
        },
        []
    );

    const clearError = useCallback(() => setError(null), []);

    return { loading, request, error, clearError }
};