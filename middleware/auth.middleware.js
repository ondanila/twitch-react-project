const jwt = require('jsonwebtoken');
const config = require('config');

module.exports = (req, res, next) => {
   if (req.method === 'OPTIONS') {
      return next();
   }
   try {
      const token = req.headers.authorization.split(' ')[1]; // "Bearer TOKEN"
      if (!token) {
         res.status(401).json({message: 'Для выполнения запроса необходима авторизация'});
      }
      const secret = config.get('jwtSecret');
      req.user = jwt.decode(token, {secret});
      
      next();
   } catch (e) {
      console.log(e);
      res.status(401).json({message: e.message || 'Для выполнения запроса необходима авторизация'});
   }
};